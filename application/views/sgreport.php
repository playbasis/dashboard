
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>stylesheet/sgreport.css" />
<script type="text/javascript" src="<?php echo base_url();?>javascript/sgreport.js"></script>

<script>
function showDashboard(elemId, link) {
  $('.dashboard_iframe').hide();
  $('#' + elemId).prop('src', link);
  $('#' + elemId).show();
}
</script>
<div id="content" class="span10">
  <div class="row-fluid sgreport">

    <div style="z-index: 100;position: relative; top: 0px;width: 100%; height: 100px;">
      <a class="btn" href="/">Back to Console</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?php
      foreach ($dashboards as $d) { 
      ?>
        <button class="btn" onclick="showDashboard('<?php echo $d["_id"];?>', '<?php echo $d["link"];?>')"><?php echo $d["name"];?></button>
      <?php
      } 
      ?>
    </div>

    <?php foreach ($dashboards as $d) { ?>
      <iframe id="<?php echo $d["_id"];?>" class="dashboard_iframe" style="display: none; margin-top: 20px;" height="100%" width="100%"></iframe>
    <?php }  ?>

  </div>
</div>

