<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skillboard_model extends MY_Model
{
    public function createSystemBoard($data ) {
        $this->set_site_mongodb($this->session->userdata('site_id'));
        $insert_data = array(
            'client_id' => $data['client_id'],
            'site_id' => $data['site_id'],
            'name' => $data['name'],
            'description' => $data['description'],
            'status' => true,
            'date_added' => new MongoDate(),
            'date_modified' => new MongoDate()
        );
        $insert = $this->mongo_db->insert('playbasis_skillboard_to_client', $insert_data);
        return $insert;
    }

    public function addSystemBadge($data)
    {
        $insert_data = array(
            'skillboard_id' => $data['skillboard_id'],
            'client_id' => $data['client_id'],
            'site_id' => $data['site_id'],
            'badge_id' => $data['badge_id'],
            'date_added' => new MongoDate(),
            'date_modified' => new MongoDate()
        );
        $insert = $this->mongo_db->insert('playbasis_skillboard_badges_to_client', $insert_data);
        return $insert;
    }

    public function getSystemBadgesFromBoard($data)
    {
        $this->mongo_db->where('skillboard_id', new MongoId($data['skillboard_id']));
        return $this->mongo_db->get("playbasis_skillboard_badges_to_client");
    }

    public function addSystemBadgeToPlayer($data)
    {
        $insert_data = array(
            'skillboard_id' => new MongoId($data['skillboard_id']),
            'client_id' => new MongoId($data['client_id']),
            'site_id' => new MongoId($data['site_id']),
            'badge_id' => new MongoId($data['badge_id']),
            'amount' => $data['amount'],
            'player_id' => new MongoId($data['player_id']),
            'date_added' => new MongoDate(),
            'date_modified' => new MongoDate()
        );
        $insert = $this->mongo_db->insert('playbasis_skillboard_badges_to_player', $insert_data);
        return $insert;
    }

    public function getPlayerBadgesFromSystemBoard($data)
    {
        $this->mongo_db->where('skillboard_id', new MongoId($data['skillboard_id']));
        $this->mongo_db->where('player_id', new MongoId($data['player_id']));
        return $this->mongo_db->get("playbasis_skillboard_badges_to_player");
    }
}

?>