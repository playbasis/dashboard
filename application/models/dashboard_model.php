<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends MY_Model
{
    public function getDashboards()
    {
        return $this->mongo_db->get("playbasis_2gen_dashboards");
    }
}