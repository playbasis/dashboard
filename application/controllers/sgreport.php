<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MY_Controller.php';

class Sgreport extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Dashboard_model');
        if (!$this->User_model->isLogged()) {
            redirect('/login', 'refresh');
        }
    }
    
    public function home()
    {
        $this->data['dashboards'] = $this->Dashboard_model->getDashboards();
        $this->data['main'] = 'sgreport';
        $this->render_page('template');
    }
}